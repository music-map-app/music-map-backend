package com.musicmap.pam.util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.crs.ProjectedCRS;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import javax.measure.Measure;
import javax.measure.quantity.Length;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

/**
 * Created by dylanhumphrey on 6/30/17.
 */
public class GeometryUtils {

    public static Geometry createCircleWithRadiusAtPoint(Double radius, Double latitude, Double longitude) {
        GeometryFactory factory = new GeometryFactory();
        Point point = factory.createPoint(new Coordinate(longitude, latitude));
        Measure<Double, Length> dist = Measure.valueOf(radius, NonSI.MILE);
        return GeometryUtils.bufferPoint(dist, DefaultGeographicCRS.WGS84, point);
    }

    private static Geometry bufferPoint(Measure<Double, Length> distance, CoordinateReferenceSystem origCRS, Geometry geom) {
        Geometry pGeom = geom;
        MathTransform toTransform, fromTransform = null;
        Unit<Length> unit = distance.getUnit();
        if (!(origCRS instanceof ProjectedCRS)) {

            double x = geom.getCoordinate().x;
            double y = geom.getCoordinate().y;

            String code = "AUTO:42001," + x + "," + y;
            CoordinateReferenceSystem auto;
            try {
                auto = CRS.decode(code);
                toTransform = CRS.findMathTransform(DefaultGeographicCRS.WGS84, auto);
                fromTransform = CRS.findMathTransform(auto, DefaultGeographicCRS.WGS84);
                pGeom = JTS.transform(geom, toTransform);
                unit = SI.METER;
            } catch (MismatchedDimensionException | TransformException | FactoryException e) {
                e.printStackTrace();
            }

        } else {
            unit = (Unit<Length>) origCRS.getCoordinateSystem().getAxis(0).getUnit();
        }

        Geometry out = pGeom.buffer(distance.doubleValue(unit));
        Geometry retGeom = out;
        if (!(origCRS instanceof ProjectedCRS)) {
            try {
                retGeom = JTS.transform(out, fromTransform);

            } catch (MismatchedDimensionException | TransformException e) {
                e.printStackTrace();
            }
        }
        return retGeom;
    }
}
