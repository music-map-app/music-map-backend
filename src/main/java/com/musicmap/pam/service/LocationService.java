package com.musicmap.pam.service;

import com.musicmap.pam.model.entity.Location;
import com.musicmap.pam.model.entity.Playlist;
import com.musicmap.pam.repository.LocationRepository;
import com.musicmap.pam.repository.PlaylistRepository;
import com.musicmap.pam.util.GeometryUtils;
import org.opengis.referencing.FactoryException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by dylanhumphrey on 6/28/17.
 */

@Service
public class LocationService {

    private final LocationRepository locationRepository;
    private final PlaylistRepository playlistRepository;

    public LocationService(LocationRepository locationRepository, PlaylistRepository playlistRepository) {
        this.locationRepository = locationRepository;
        this.playlistRepository = playlistRepository;
    }

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public Location addLocation(Location location) {
        return locationRepository.save(location);
    }

    public List<Location> getLocationsWithin(Double latitude, Double longitude, Double radius) throws FactoryException {
        return locationRepository.findByPointWithin(GeometryUtils.createCircleWithRadiusAtPoint(radius, latitude, longitude));
    }

    public void deleteLocation(Long id) {
        locationRepository.delete(id);
    }

    public Playlist insertPlaylistIntoLocation(Playlist playlist, Long id) {
        Location location = locationRepository.findOne(id);
        location.getPlaylists().add(playlist);
        playlistRepository.save(playlist);
        locationRepository.save(location);
        return playlist;
    }
}
