package com.musicmap.pam.service;

import com.wrapper.spotify.Api;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.methods.CurrentUserRequest;
import com.wrapper.spotify.methods.PlaylistRequest;
import com.wrapper.spotify.methods.UserPlaylistsRequest;
import com.wrapper.spotify.methods.UserRequest;
import com.wrapper.spotify.methods.authentication.AuthorizationCodeGrantRequest;
import com.wrapper.spotify.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by dylanhumphrey on 7/5/17.
 */

@Service
@Scope("prototype")
public class SpotifyService {
    private final Api spotifyApi;

    @Autowired
    public SpotifyService(Api spotifyApi) {
        this.spotifyApi = spotifyApi;
    }

    public String getAuthenticationURL() {
        final List<String> scopes = Arrays.asList("user-read-private");
        return spotifyApi.createAuthorizeURL(scopes).build().toStringWithQueryParameters();

    }

    public boolean getAccessToken(String code) throws WebApiException {
        final AuthorizationCodeGrantRequest credentialsRequest = spotifyApi.authorizationCodeGrant(code).build();
        try {
            final AuthorizationCodeCredentials credentials = credentialsRequest.get();
            spotifyApi.setAccessToken(credentials.getAccessToken());
            spotifyApi.setRefreshToken(credentials.getRefreshToken());
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public Optional<Playlist> getPlaylist(String userId, String playlistId) throws WebApiException {
        final PlaylistRequest request = spotifyApi.getPlaylist(userId, playlistId).build();
        try {
            return Optional.of(request.get());
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    public Optional<List<SimplePlaylist>> getUserPlaylists(String userId) throws WebApiException {
        final UserPlaylistsRequest request = spotifyApi.getPlaylistsForUser(userId).build();
        try {
            final Page<SimplePlaylist> playlistPage = request.get();
            return Optional.of(playlistPage.getItems());
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    public Optional<User> me() throws WebApiException {
        final CurrentUserRequest request = spotifyApi.getMe().build();
        try {
            return Optional.of(request.get());
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}
