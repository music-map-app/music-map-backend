package com.musicmap.pam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by dylanhumphrey on 6/25/17.
 */

@SpringBootApplication
public class PamApplication {
    public static void main(String[] args) {
        SpringApplication.run(PamApplication.class, args);
    }
}
