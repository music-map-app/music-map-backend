package com.musicmap.pam.repository;


import com.musicmap.pam.model.entity.Location;
import com.vividsolutions.jts.geom.Geometry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by dylanhumphrey on 6/28/17.
 */

public interface LocationRepository extends JpaRepository<Location, Long> {

    @Query("select l from Location l where within(l.point, :geometry) = true")
    List<Location> findByPointWithin(@Param("geometry") Geometry geometry);
}
