package com.musicmap.pam.repository;

import com.musicmap.pam.model.entity.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by dylanhumphrey on 7/2/17.
 */
public interface PlaylistRepository extends JpaRepository<Playlist, Long>{
}
