package com.musicmap.pam.controller;

import com.musicmap.pam.exception.HttpException;
import com.musicmap.pam.service.SpotifyService;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.models.Playlist;
import com.wrapper.spotify.models.SimplePlaylist;
import com.wrapper.spotify.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by dylanhumphrey on 7/5/17.
 */

@RestController
@CrossOrigin
@RequestMapping("/api/spotify")
public class SpotifyController {

    private final SpotifyService spotifyService;

    @Autowired
    public SpotifyController(SpotifyService spotifyService) {
        this.spotifyService = spotifyService;
    }

    @RequestMapping("/authenticationURL")
    public ResponseEntity<?> getAuthenticationURL() {
        return ResponseEntity.ok(spotifyService.getAuthenticationURL());
    }

    @RequestMapping("/authorize")
    public ResponseEntity<?> authenticate(@RequestParam String code) {
        try {
            final boolean success = spotifyService.getAccessToken(code);
            if (success) {
                return ResponseEntity.ok("Authentication was successful");
            }
            final String reason = "There was an IO error when trying to do the authentication request";
            return generateInternalServerResponse(reason);
        } catch (WebApiException e) {
            return generateBadRequestResponse(e);
        }
    }

    @RequestMapping("/me")
    public ResponseEntity<?> me() {
        try {
            final Optional<User> me = spotifyService.me();
            if (me.isPresent()){
                return ResponseEntity.ok(me.get());
            }
            final String reason = "There was an IO error when trying to do the user request";
            return generateInternalServerResponse(reason);
        } catch (WebApiException e) {
            return generateBadRequestResponse(e);
        }
    }

    @RequestMapping("/playlists")
    public ResponseEntity<?> playlists(@RequestParam String userId) {
        try {
            final Optional<List<SimplePlaylist>> playlists = spotifyService.getUserPlaylists(userId);
            if (playlists.isPresent()) {
                return ResponseEntity.ok(playlists.get());
            }
            final String reason = "There was an IO error when trying to do the playlists request";
            return generateInternalServerResponse(reason);
        } catch (WebApiException e) {
            return generateBadRequestResponse(e);
        }
    }

    @RequestMapping("/playlist")
    public ResponseEntity<?> playlist(@RequestParam String userId, @RequestParam String playlistId) {
        try {
            final Optional<Playlist> playlist = spotifyService.getPlaylist(userId, playlistId);
            if (playlist.isPresent()) {
                return ResponseEntity.ok(playlist.get());
            }
            final String reason = "There was an IO error when trying to do the playlist request";
            return generateInternalServerResponse(reason);
        } catch (WebApiException e){
            return generateBadRequestResponse(e);
        }
    }

    private ResponseEntity<?> generateInternalServerResponse(String message) {
        return ResponseEntity.status(500).body(new HttpException(HttpStatus.INTERNAL_SERVER_ERROR, message).toJsonMap());
    }

    private ResponseEntity<?> generateBadRequestResponse(WebApiException e) {
        return ResponseEntity.badRequest().body(new HttpException(HttpStatus.BAD_REQUEST, e.getMessage()).toJsonMap());
    }
}
