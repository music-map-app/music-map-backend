package com.musicmap.pam.controller;

import com.musicmap.pam.controller.request.LocationInsertRequest;
import com.musicmap.pam.controller.request.PlaylistInsertRequest;
import com.musicmap.pam.exception.HttpException;
import com.musicmap.pam.model.entity.Location;
import com.musicmap.pam.service.LocationService;
import org.opengis.referencing.FactoryException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dylanhumphrey on 6/28/17.
 */

@RestController
@CrossOrigin
@RequestMapping("/api/locations")
public class LocationController {

    private final LocationService locationService;

    public LocationController(LocationService service) {
        this.locationService = service;
    }

    @RequestMapping("/all")
    public ResponseEntity<?> getAllLocations() {
        return ResponseEntity.ok(locationService.getAllLocations());
    }

    @RequestMapping
    public ResponseEntity<?> getLocations(@RequestParam Double latitude,
                                          @RequestParam Double longitude,
                                          @RequestParam Double radius) {
        try{
            List<Location> locations = locationService.getLocationsWithin(latitude, longitude, radius);
            return ResponseEntity.ok(locations);
        }catch (FactoryException e){
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            HttpException exception = new HttpException(status, e.getMessage());
            return ResponseEntity.status(status).body(exception.toJsonMap());
        }

    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> insertLocation(@RequestBody LocationInsertRequest request) {
        try {
            return ResponseEntity.ok(locationService.addLocation(request.asLocation()));
        }catch (DataAccessException e){
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            HttpException exception = new HttpException(status, e.getMostSpecificCause().getMessage());
            return ResponseEntity.status(status).body(exception.toJsonMap());
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> insertPlaylistIntoLocation(@RequestBody PlaylistInsertRequest request) {
        return ResponseEntity.ok(locationService.insertPlaylistIntoLocation(request.asPlaylist(), request.getLocationId()));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteLocation(@PathVariable Long id) {
        try {
            locationService.deleteLocation(id);
            return ResponseEntity.ok("Successfully deleted location");
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e);
        }
    }
}
