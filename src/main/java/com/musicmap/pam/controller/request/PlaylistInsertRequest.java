package com.musicmap.pam.controller.request;

import com.musicmap.pam.model.entity.Playlist;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by dylanhumphrey on 7/2/17.
 */

@Data
@AllArgsConstructor
public class PlaylistInsertRequest {
    private String name;
    private Long locationId;
    private String userId;
    private String playlistId;

    public Playlist asPlaylist() {
        return new Playlist(userId, playlistId, name);
    }
}
