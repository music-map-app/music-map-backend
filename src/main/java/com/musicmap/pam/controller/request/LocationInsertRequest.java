package com.musicmap.pam.controller.request;

import com.musicmap.pam.model.entity.Location;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by dylanhumphrey on 7/2/17.
 */

@Data
@AllArgsConstructor
public class LocationInsertRequest {
    private Double latitude;
    private Double longitude;
    private String title;

    public Location asLocation() {
        GeometryFactory factory = new GeometryFactory();
        Point p = factory.createPoint(new Coordinate(this.longitude, this.latitude));
        return new Location(this.title, p);
    }
}
