package com.musicmap.pam.exception;

import org.springframework.http.HttpStatus;

import javax.xml.ws.http.HTTPException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dylanhumphrey on 6/28/17.
 */
public class HttpException extends Exception {

    private int status;
    private String statusText;

    public HttpException() {
        status = 500;
        statusText = "Internal Server Error";
    }

    public HttpException(HttpStatus status, String message) {
        super(message);
        this.status = status.value();
        this.statusText = status.getReasonPhrase();
    }

    public int getStatus() {
        return status;
    }

    public String getStatusText() {
        return statusText;
    }

    public Map<String, Object> toJsonMap(){
        Map<String, Object> map = new HashMap<>();
        map.put("status", this.getStatus());
        map.put("statusText", this.getStatusText());
        map.put("message", this.getMessage());
        return map;
    }
}
