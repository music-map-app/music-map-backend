package com.musicmap.pam.model.serialization;

/**
 * Created by dylanhumphrey on 6/29/17.
 */

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.vividsolutions.jts.geom.Point;

public class PointToJsonSerializer extends JsonSerializer<Point> {

    @Override
    public void serialize(Point value, JsonGenerator jgen,
                          SerializerProvider provider) throws IOException, JsonProcessingException {
        Map<String, Double> map = new HashMap<>();
        try
        {
            if(value != null) {
                double lat = value.getY();
                double lon = value.getX();
                map.put("latitude", lat);
                map.put("longitude", lon);
                jgen.writeObject(map);
            }
        }
        catch(Exception e) {
            jgen.writeString("Serialization failed");
        }

    }

}