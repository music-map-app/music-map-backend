package com.musicmap.pam.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.musicmap.pam.model.serialization.JsonToPointDeserializer;
import com.musicmap.pam.model.serialization.PointToJsonSerializer;
import com.vividsolutions.jts.geom.Point;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by dylanhumphrey on 6/28/17.
 */

@Data
@Entity
public class Location {

    @Id @GeneratedValue
    private Long id;

    @Version
    private int version;

    @Temporal(TemporalType.DATE)
    private Date creationDate;

    private String title;

    @Column(columnDefinition = "POINT")
    @JsonSerialize(using = PointToJsonSerializer.class)
    @JsonDeserialize(using = JsonToPointDeserializer.class)
    private Point point;

    @OneToMany
    private Set<Playlist> playlists;

    protected Location() {
    };

    public Location(String title, Point point) {
        this.title = title;
        this.point = point;
        this.creationDate = new Date();
        playlists = new HashSet<>();
    }
}
