package com.musicmap.pam.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by dylanhumphrey on 7/2/17.
 */

@Entity
@Data
public class Playlist {

    @Id @GeneratedValue
    private Long id;

    private String userId;
    private String playlistId;

    private String name;

    protected Playlist() {

    }

    public Playlist(String userId, String playlistId, String name) {
        this.userId = userId;
        this.playlistId = playlistId;
        this.name = name;
    }
}
