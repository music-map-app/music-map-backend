#Table of Contents
1. [Model](#model)
    1. [Point Object](#point)
    2. [Playlist Object](#playlist)
    3. [Location Object](#location)
2. [Location API](#rest)
    1. [Get all locations](#gal)
    2. [Get all locations within radius](#galwr)
    3. [Add a location](#addL)
    4. [Add a playlist to a location](#addptl)
    5. [Delete location](#dl)
3. [Spotify API](#spotify)
    1. [Authentication](#auth)
    2. [Current User](#me)
    3. [Users Playlists](#up)
    4. [Specific Playlist](#sp)


# Model <a name="model"></a>

## Point Object <a name="point"></a>
| key       | type   | description |
|:---------:|:------:| ----------- |
| latitude  | Number | The latitude presented as a decimal number  |
| longitude | Number | The longitude presented as a decimal number |             

## Playlist Object <a name="playlist"></a>
| key        | type   | description |
|:----------:|:------:| ----------- |
| id         | Number | The unique id for this playlist |
| userId     | String | The spotify user who owns the playlist |
| playlistId | String | The id of this playlist for use in spotifies API |
| name       | String | The name of the playlist |

## Location object <a name="location"></a>
| key          | type                      | description |
|:------------:|:-------------------------:| ----------- |
| id           | Number                    | The unique id for this location |
| version      | Number                    | The version of this object |
| creationDate | Date                      | The date the location was created |
| title        | String                    | The title of the location |
| point        | [Point Object](#point)    | The long and lat for the location |
| playlists    | Array of [Playlist objects](#playlist) | All the playlists attached to this location |

# Rest API <a name="rest"></a>

## Get all locations <a name="gal"></a>

### Endpoint
```GET 35.197.23.213/api/locations/all```

### Response Format
On success, the HTTP status code in the response header is `200` OK
and the response body contains an array of [location objects](#location).

### Example
```$ curl -X GET "35.197.23.213/api/locations/all"```
```
[
    {
        "id": 1,
        "version": 1,
        "creationDate": "2017-06-29",
        "title": "Dylan's House",
        "point": {
            "latitude": 34.0525979,
            "longitude": -118.5213311
        },
        "playlists": [
            {
                "id": 1,
                "userId": "1234",
                "playlistId": "4321",
                "name": "Test Playlist"
            }
        ]
    },
    {
        "id": 2,
        "version": 0,
        "creationDate": "2017-06-29",
        "title": "Some Point",
        "point": {
            "latitude": 34.1525979,
            "longitude": -118.5213311
        },
        "playlists": []
    }
]
```

## Get Locations Within a Radius <a name="galwr"></a>

### Endpoint
```GET 35.197.23.213/api/locations?latitude={center latitude}&longitude={center longitude}&radius={radius in miles}```

### Request Parameters
| Parameter | Type   | Description |
| --------- | ------ | ----------- |
| latitude  | Number | The center point's latitude |
| longitude | Number | The center point's longitude |
| radius    | Number | The radius of the circle in miles |

### Response Format
On success, the HTTP status code in the response header is `200` OK
and the response body contains an array of [location objects](#location) all of which are within
the specified radius of the inputted point.

## Add a location <a name="addL"></a>

### Endpoint
```POST 35.197.23.213/api/locations```

### Request Headers

| Header Field | Value            |
| ------------ | ---------------- |
| Accept       | application/json |
| Content-Type | application/json |

### Request Body
| Data | Type   | Description |
| --------- | ------ | ----------- |
| latitude  | Number | The location's latitude |
| longitude | Number | The location's longitude |
| title    | String | The title of this location |

### Response Format
On success, the HTTP status code in the response header is `200` OK
and the response body contains the newly created location object.

### Example
```$ curl -X POST "35.197.23.213/api/locations" -H "Accept: application/json" -H "Content-Type: application/json" --data "{\"latitude\": 34.1025979, \"longitude\": -118.4013311, \"title\":\"Just another point\"}"```
```
{
    "id": 5,
    "version": 0,
    "creationDate": 1499056121841,
    "title": "Just another point",
    "point": {
        "latitude": 34.1025979,
        "longitude": -118.4013311
    },
    "playlists": []
}
```

## Add a playlist to a location <a name="addptl"></a>

### Endpoint
```PUT 35.197.23.213/api/locations```

### Request Headers

| Header Field | Value            |
| ------------ | ---------------- |
| Accept       | application/json |
| Content-Type | application/json |

### Request Body

| Data | Type   | Description |
| --------- | ------ | ----------- |
| name  | Number | The name of this playlist, specified by the user |
| locationId | Number | The id of the location this playlist is being added to |
| userId    | String | The id of the spotify api user who owns the playlist |
| playlistId | String | The id of the spotify api playlist |

### Response Format

On success, the HTTP status code in the response header is `200` OK
and the response body contains the newly added playlist object.

### Example
```$ curl -X PUT "35.197.23.213/api/locations" -H "Accept: application/json" -H "Content-Type: application/json" --data "{\"name\": \"Test Playlist\", \"locationId\": 2, \"userId\":\"1234\", \"playlistId\": \"4321\"}"```
```
{
    "id": 2,
    "userId": "1234",
    "playlistId": "4321",
    "name": "Test Playlist"
}
```

## Delete Location <a name="dl"></a>

### Endpoint
```DELETE 35.197.23.213/api/locations/{id}```

### Path Variable

| Data | Type   | Description |
| --------- | ------ | ----------- |
| id  | Number | The unique ID of the playlist to delete|

### Response Format

On success, the HTTP status code in the response header is `200` OK.
On failure, the HTTP status code in the response header is `500` INTERNAL_SERVER_ERROR.

### Example

```$ curl -X DELETE "35.197.23.213/api/locations/1"```

```
"Successfully deleted playlist"
```


# Spotify API <a name="spotify"></a>

## Authentication <a name="auth"></a>

### Endpoint
```GET 35.197.23.213/api/spotify/authenticationURL```

### Response Format

On success, the request will return a URL which should be opened in a separate window. In this window, the authentication
will take place as in the app requesting permissions from the user via the spotify API. On acceptance, the spotify API will
automatically send a request to the backend endpoint `GET 35.197.23.213/api/spotify/authorize` with an attached code. Using this code, the backend will retrieve an `Access Token` and a `Refresh Token`, with which all of the following calls can be made.

### Example
```$ curl -X GET "35.197.23.213/api/spotify/authenticationURL"```

```
https://accounts.spotify.com:443/authorize?client_id=c26b3e6e45a648d682c2aae39526dcc1&response_type=code&redirect_uri=http://35.197.23.213/api/spotify/authorize&scope=user-read-private
```

## Current User <a name="me"></a>

### Endpoint
```GET 35.197.23.213/api/spotify/me```

### Response Format

On success, the HTTP status code in the response header is `200` OK and the response body
will contain the current user in JSON format.

### Example
```$ curl -X GET "35.197.23.213/api/spotify/me"```
```
{
    "displayName": "Dylan Humphrey",
    "email": null,
    "externalUrls": {
        "externalUrls": {
            "spotify": "https://open.spotify.com/user/ceece101"
        }
    },
    "followers": {
        "href": null,
        "total": 14
    },
    "href": "https://api.spotify.com/v1/users/ceece101",
    "id": "ceece101",
    "country": "US",
    "images": [
        {
            "height": null,
            "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/12439054_1285175568165580_2011727878298472938_n.jpg?oh=31f4fabb32de18f291055dd1149411ee&oe=59D585E7",
            "width": null
        }
    ],
    "product": "PREMIUM",
    "type": "USER",
    "uri": "spotify:user:ceece101"
}
```

## Users Playlists <a name="up"></a>

### Endpoint
```GET 35.197.23.213/api/spotify/playlists?userId={the user id}```

### Request Parameters
| Parameter | Type   | Description |
| --------- | ------ | ----------- |
| userId  | String | The user id |

### Response Format

On success, the HTTP status code in the response header is `200` OK and the response body
will contain the specified users playlists in JSON format.

### Example
```$ curl -X GET "35.197.23.213/api/spotify/playlists?userId=ceece101"```
```
[
    {
        "collaborative": false,
        "externalUrls": {
            "externalUrls": {
                "spotify": "http://open.spotify.com/user/121410021/playlist/4FO2WXjS922s0RheaTYPZK"
            }
        },
        "href": "https://api.spotify.com/v1/users/121410021/playlists/4FO2WXjS922s0RheaTYPZK",
        "id": "4FO2WXjS922s0RheaTYPZK",
        "images": [
            {
                "height": null,
                "url": "https://u.scdn.co/images/pl/default/6eca714927fc4880be2a83e5be2a1fc2a10e7eac",
                "width": null
            }
        ],
        "owner": {
            "displayName": null,
            "email": null,
            "externalUrls": {
                "externalUrls": {
                    "spotify": "http://open.spotify.com/user/121410021"
                }
            },
            "followers": {
                "href": null,
                "total": 0
            },
            "href": "https://api.spotify.com/v1/users/121410021",
            "id": "121410021",
            "country": null,
            "images": null,
            "product": null,
            "type": "USER",
            "uri": "spotify:user:121410021"
        },
        "name": "Songs That Get Drunk White Girls Excited",
        "publicAccess": true,
        "tracks": {
            "href": "https://api.spotify.com/v1/users/121410021/playlists/4FO2WXjS922s0RheaTYPZK/tracks",
            "total": 359
        },
        "type": "PLAYLIST",
        "uri": "spotify:user:121410021:playlist:4FO2WXjS922s0RheaTYPZK"
    },
    {
        "collaborative": false,
        "externalUrls": {
            "externalUrls": {
                "spotify": "http://open.spotify.com/user/128495535/playlist/4J03yVwkXcBMmPb5oWkcmV"
            }
        },
        "href": "https://api.spotify.com/v1/users/128495535/playlists/4J03yVwkXcBMmPb5oWkcmV",
        "id": "4J03yVwkXcBMmPb5oWkcmV",
        "images": [
            {
                "height": 640,
                "url": "https://mosaic.scdn.co/640/529b2d1e33d2715ccf528542471d3d9c09aa4e716c2344165df1579d9d5ea796577144fb68ac3b9f06c7ba9e48b9bee598200277040eb7abaed0b3c392af9b238044773de8eb89c569edf14333b60ce6",
                "width": 640
            },
            {
                "height": 300,
                "url": "https://mosaic.scdn.co/300/529b2d1e33d2715ccf528542471d3d9c09aa4e716c2344165df1579d9d5ea796577144fb68ac3b9f06c7ba9e48b9bee598200277040eb7abaed0b3c392af9b238044773de8eb89c569edf14333b60ce6",
                "width": 300
            },
            {
                "height": 60,
                "url": "https://mosaic.scdn.co/60/529b2d1e33d2715ccf528542471d3d9c09aa4e716c2344165df1579d9d5ea796577144fb68ac3b9f06c7ba9e48b9bee598200277040eb7abaed0b3c392af9b238044773de8eb89c569edf14333b60ce6",
                "width": 60
            }
        ],
        "owner": {
            "displayName": null,
            "email": null,
            "externalUrls": {
                "externalUrls": {
                    "spotify": "http://open.spotify.com/user/128495535"
                }
            },
            "followers": {
                "href": null,
                "total": 0
            },
            "href": "https://api.spotify.com/v1/users/128495535",
            "id": "128495535",
            "country": null,
            "images": null,
            "product": null,
            "type": "USER",
            "uri": "spotify:user:128495535"
        },
        "name": "Winter is Coming",
        "publicAccess": true,
        "tracks": {
            "href": "https://api.spotify.com/v1/users/128495535/playlists/4J03yVwkXcBMmPb5oWkcmV/tracks",
            "total": 163
        },
        "type": "PLAYLIST",
        "uri": "spotify:user:128495535:playlist:4J03yVwkXcBMmPb5oWkcmV"
    }...
```

## Specific Playlist <a name="sp"></a>

### Endpoint
```GET 35.197.23.213/api/spotify/playlist?userId={the user's id}&playlistId={the playlist id}```

### Request Parameters
| Parameter | Type   | Description |
| --------- | ------ | ----------- |
| userId  | String | The user id |
| playlistId | String | the playist id |

### Response Format

On success, the HTTP status code in the response header is `200` OK and the response body
will contain the specified playlist in JSON format.

### Example
```$ curl -X GET "35.197.23.213/api/spotify/playlist?userId=ceece101&playlistId=0g4ytX7n95s6T8wINUbK1Q"```
```
{
        "collaborative": false,
        "externalUrls": {
            "externalUrls": {
                "spotify": "http://open.spotify.com/user/ceece101/playlist/0g4ytX7n95s6T8wINUbK1Q"
            }
        },
        "href": "https://api.spotify.com/v1/users/ceece101/playlists/0g4ytX7n95s6T8wINUbK1Q",
        "id": "0g4ytX7n95s6T8wINUbK1Q",
        "images": [
            {
                "height": null,
                "url": "https://u.scdn.co/images/pl/default/7d42e8d645b2e800dadbbe66b2e60006bec2411a",
                "width": null
            }
        ],
        "owner": {
            "displayName": null,
            "email": null,
            "externalUrls": {
                "externalUrls": {
                    "spotify": "http://open.spotify.com/user/ceece101"
                }
            },
            "followers": {
                "href": null,
                "total": 0
            },
            "href": "https://api.spotify.com/v1/users/ceece101",
            "id": "ceece101",
            "country": null,
            "images": null,
            "product": null,
            "type": "USER",
            "uri": "spotify:user:ceece101"
        },
        "name": "②🙃①⑦",
        "publicAccess": true,
        "tracks": {
            "href": "https://api.spotify.com/v1/users/ceece101/playlists/0g4ytX7n95s6T8wINUbK1Q/tracks",
            "total": 69
        },
        "type": "PLAYLIST",
        "uri": "spotify:user:ceece101:playlist:0g4ytX7n95s6T8wINUbK1Q"
    }
```
